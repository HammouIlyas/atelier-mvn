package com.example.ateliermaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtelierMavenApplication {
    private int version;
    private String name;

    @PostMapping("/home-controller")
    public String helloWorld() {
        return "hello world";
    }

    public static void main(String[] args) {
        SpringApplication.run(AtelierMavenApplication.class, args);
    }

}
